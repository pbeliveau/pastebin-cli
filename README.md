# Pastebin-cli
Bash program to retrive or publish pastebins using [ptpb.pw](https://ptpb.pw/).

## Features
- Capable of using ptpb.pw 'sunset' capacity so that your pastebins autodelete
  after a number of seconds.
- Can encrypt your paste before uploading OR, decrypt a published one. Uses
  [AES-256 via ccrypt.](http://ccrypt.sourceforge.net/)
- Only have to provide the pastebin url following 'ptpb.pw' to quickly retrieve.

## Purpose
Using plain vanilla curl for [ptpb.pw](https://ptpb.pw/) is useful but
tedious. At some point, you will want a more efficent way. Instead
of scripting it yourself, you can use this one as-is or modify it.

## Installation on Linux

``` shell
wget "https://gitlab.com/pbeliveau/pastebin-cli/raw/master/pastebin" pastebin
chmod 751 pastebin
```

## Use
### Options
x) Enter a password to encrypt the ouput in AES-256. Leave empty if you want to
   upload unencrypted output. (Need ccrypt to work)  
f) Specify the filename to output. The script will fail without this option.  
s) Make full use of the 'sunset' capacity of https://ptpb.pw/. Leave empty if you want
   your script to stay online for an unspecified period of time.  
r) Reverse the use. -x decrypts, -f is now optional to save the output locally,
   and -s is null. You have to specify the code following the URL (https://ptpb.pw/${CODE}. See example.  
h) Show this message output.  

### Examples
Encrypt output of specified file:

        pastebin -f test.txt -x "verysecurepassword"

Encrypt, and set an autodelete in seconds (60 in this case):

        pastebin -f test.txt -x "verysecurepassword" -s 60

Reverse use:

        pastebin -r "xY34" -x "passwordtodecrypt" -f "filetosave.txt"
